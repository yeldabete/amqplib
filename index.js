'use strict';
const promise =  require('./lib/promise');
const callback = require('./lib/callback');

exports = module.exports = {
	promise, callback
};

