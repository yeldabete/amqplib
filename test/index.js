'use strict';
require('dotenv').config();

const runsv = require('runsv');
const {callback, promise} = require('..');

const assert = require('assert');
const CI_AMQP_URL = process.env.CI_AMQP_URL;

assert(CI_AMQP_URL, 'CI_AMQP_URL env var required');

describe('runsv-amqplib',function(){
	describe('#promise API', function(){
		it('should be able to connect and disconnect', async function(){
			const service = promise(CI_AMQP_URL);
			const sv = runsv.create().async();
			sv.addService(service);
			await sv.start();
			const  {amqplib} = sv.getClients();
			const chan = await amqplib.createChannel();
			await chan.close();
			await sv.stop();
		});

	});
	describe('#callback API',function(){
		it('should be able to connect and disconnect',function(done){
			const service = callback(CI_AMQP_URL);
			const sv = runsv.create();
			sv.addService(service);
			sv.start(function(err, client){
				if(err){
					return done(err);
				}
				const  {amqplib} = client;
				// Test will never finish if the connection is not closed
				amqplib.on('close', done);
				amqplib.createChannel(function(err, chan){
					if(err){
						return done(err);
					}
					chan.close();
					sv.stop(function(err){
						if(err){
							return done(err);
						}
					});
				});
			});
		});
	});
});
